# AuthApp #

AuthApp is a sample docker project demonstrating an authentication API written in Python, driven by a front-end UI build in HTML and Javascript. 

A full writeup of the project can be found on my blog. 

### Required software:
- docker
- docker-compose

### Getting going
- Download the repo
- In the root of the repo, run `docker-compose build`
- Bring up the project with `docker-compose up -d`
- Access the UI at `https://localhost:8088/`

The default password is `admin` and `changeme`. 

Once logged in for the first time, passwords can be changed to whatever you wish. In the event you loose the admin password, you can reset it with the following command: 
`docker exec -it authapp_api python3 /auth_app/pymongo_reset_admin.py`