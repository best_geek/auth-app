from logging import debug
import re

import pymongo
from pymongo import auth
from pymongo_mod.pymongo_functions import *
from pymongo_mod.pymongo_get_user_record import *
from pymongo_mod.pymongo_update_user_records import *
from pymongo_config import *
from auth_handle.auth import *
from auth_handle.token_functions import *
from auth_handle.defaults import *

import json

from bson.objectid import ObjectId
import json
import time


def authenticate_user(http_auth_header={}):
    """authenticates user. Will document as I go"""

    auth_by_token = False
    auth_by_password = False

    # generate a default error
    result = {"result": "fail", "failure_reason": "unspecified error in authentication"}

    #### Pre check before attempting any DB interaction
    if not http_auth_header:
        result = {"result": "fail", "errors": "no authentication information sent in header 'auth'"}
        return result

    # try convertion to JSON
    try:
        http_auth_header = json.loads(http_auth_header)
    except:
        result = {"result": "fail", "errors": "incorrect authentication format"}
        return result

    upstatus = checkconnect(MONGO_URL)
    if upstatus is False:
        result = {"result": "fail", "errors": "authention db unavailable"}
        logging.error(f"MongoDB backend unavailable with {MONGO_URL}")
        return result
    else:
        logging.info("MongoDB connection established")

    # get our base required variables
    userid = http_auth_header.get("username", None)
    password = http_auth_header.get("password", None)
    token = http_auth_header.get("token", None)

    # get mongodb record
    user_record = get_user_record_by_userid(userid=userid)
    if not user_record:
        result = {"result": "fail", "failure_reason": "Incorrect credentials"}
        return result

    ### begin authenticaiton now we have a valid credential

    # update authenticaiton methods if we have a token
    if token:
        auth_by_token = True
        logging.info(f"Attempt token auth for user: {userid}")

    # try token first
    if user_record and auth_by_token:
        token_valid = see_if_token_valid(pymongo_record=user_record, submitted_token=token)

        if token_valid == "valid":
            logging.debug(f"user: '{userid}' submitted token is valid")

            new_token_fields = gen_new_token()
            extracted_token = list(new_token_fields.keys())[0]

            updated_tokens = {**user_record["tokens"], **new_token_fields}  # merges old auth tokens with new ones by combining dict
            update_user_record(userid=userid, update_fields={"tokens": updated_tokens})  # update user tokens

            # now we must remove the old token now we know it was valid
            # this stops token buildup in the mongo db
            # we must get the renew getting the record since we've updated it by adding new tokens
            user_record = get_user_record_by_userid(userid=userid)
            cleaned_tokens = return_record_popped_token(pymongo_record=user_record, submitted_token=token)

            # only update if function returned data an not nonetype
            if cleaned_tokens:
                update_user_record(userid=userid, update_fields={"tokens": cleaned_tokens})  # update user tokens

            logging.info(f"User: '{userid}' signed in with token")

            return_auth_data = {"username": userid, "token": extracted_token, "expires": new_token_fields[extracted_token]["auth_token_valid_to"]}
            result = {"result": "success", "auth_data": return_auth_data}
            return result

        if token_valid == "expired":

            user_record = get_user_record_by_userid(userid=userid)  # refresh data
            # remove the token if expired
            cleaned_tokens = return_record_popped_token(pymongo_record=user_record, submitted_token=token)

            if cleaned_tokens:
                update_user_record(userid=userid, update_fields={"tokens": cleaned_tokens})  # update user tokens

        if not token_valid:
            logging.info(f"user: '{userid}' submitted invalid token for authentication")
            result = {"result": "fail", "failure_reason": "incorrect_token_or_pass"}

    # default back to password authentication as any token checks failed at this point
    auth_by_password = True

    if user_record and auth_by_password and password:
        print("Password authentication")
        try_pass = secure_hash_password(plain_text=password, pass_salt=user_record["auth_salt"])

        if try_pass == user_record["auth_password"]:

            # make new tokens
            new_token_fields = gen_new_token()

            # there's only one key in our dict containing the token so we can always assume it will be the
            # first index in a list of keys
            # see token_functions.py for data structure
            # must convert .keys() to type list, even though it's already list? LOL
            extracted_token = list(new_token_fields.keys())[0]

            # update our tokens if we have exisiting tokens or just replace with our one :
            if user_record["tokens"]:  # otherwise none type
                updated_tokens = {**user_record["tokens"], **new_token_fields}  # merges old auth tokens with new ones by combining dict
                update_user_record(userid=userid, update_fields={"tokens": updated_tokens})
            else:
                update_user_record(userid=userid, update_fields={"tokens": new_token_fields})

            logging.info(f"User: '{userid}' signed in with password")

            # return data so javascript can make an authentication cookie
            return_auth_data = {"username": userid, "token": extracted_token, "expires": new_token_fields[extracted_token]["auth_token_valid_to"]}
            result = {"result": "success", "auth_data": return_auth_data}
            return result
        else:
            result = {"result": "fail", "failure_reason": "Incorrect_credentials"}
            return result

    return result


def signout_specific_token(userid="", token=""):
    """gets a mongodb record for a user. If the user has an authentication token that macthes the one
    submitted in the function param, it gets removed. When calling this function, using the most recent
    authentication token will ensure there's no active token from the user's browser/asset and they cannot
    continue to re-authenticate from that assset without getting a new token from password/username auth"""
    user_record = get_user_record_by_userid(userid=userid)
    if user_record:
        cleaned_tokens = return_record_popped_token(pymongo_record=user_record, submitted_token=token)

        # cleaned token returns none if wasn't in list. Don't want to replace tokens with 'None' type
        if cleaned_tokens:
            update_user_record(userid=userid, update_fields={"tokens": cleaned_tokens})  # update user tokens

        return {"result": "success"}

    return {"result": "fail"}


def get_safe_user_info_by_record(userid=""):
    """gets a mongodb record for a user. If the user has an authentication token that macthes the one
    submitted in the function param, it gets removed. When calling this function, using the most recent
    authentication token will ensure there's no active token from the user's browser/asset and they cannot
    continue to re-authenticate from that assset without getting a new token from password/username auth"""
    user_record = get_user_record_by_userid(userid=userid)
    if user_record:
        safe_return_info = {}
        safe_return_info["userid"] = user_record["userid"]
        safe_return_info["fullname"] = user_record["fullname"]
        safe_return_info["email"] = user_record["email"]
        safe_return_info["fullname"] = user_record["fullname"]
        safe_return_info["roles"] = user_record["roles"]

        return safe_return_info

    return {"result": "fail"}


def change_password_by_id(userid="", plain_text_pass=""):
    """changes password by userid. Will get user record and determine
    if user is in db. Will also see if salt already exists in db and create
    one if not. This way this function can be used to create passwords as well as change"""

    result = {"result": "fail", "failure_reason": "unspecified_error_in_pass_change"}

    if not userid or not plain_text_pass:
        return {"result": "fail", "failure_reason": "insufficient_info_supplied"}

    user_record = get_user_record_by_userid(userid=userid)
    if not user_record:
        return {"result": "fail", "failure_reason": "invalid_user"}

    # set default for salt checks
    need_new_salt = False

    # check to see user has salt
    if not user_record["auth_salt"]:
        need_new_salt = True

    # if not need salt, get our fields. Note keep these the same as the mongodb schema
    if not need_new_salt:
        auth_salt = user_record["auth_salt"]
    else:
        auth_salt = gen_salt()

    generated_pass = secure_hash_password(plain_text=plain_text_pass, pass_salt=auth_salt)

    # update our mongodb record
    update_fields = {"auth_password": generated_pass, "auth_salt": auth_salt}

    logging.debug(update_fields)

    if generated_pass:
        update_user_record(userid=userid, update_fields=update_fields)  # update user tokens
        return {"result": "success"}

    # fallback with out default error at begining of function
    return result


def create_new_user(new_user_fields=""):
    """changes password by userid. Will get user record and determine
    if user is in db. Will also see if salt already exists in db and create
    one if not. This way this function can be used to create passwords as well as change"""

    # set default

    result = {"result": "fail", "failure_reason": "unspecified_error_in_user_create"}

    if not new_user_fields:
        return {"result": "fail", "failure_reason": "insufficient_info_supplied"}

    # try loading as JSON
    try:
        new_user_fields = json.loads(new_user_fields)
    except:
        return result

    # make new def entry
    new_entry = gen_default_usr_entry()

    # do email
    if new_user_fields.get("email", None):
        if not "@" in new_user_fields["email"]:
            return {"result": "fail", "failure_reason": "invalid_email"}
        else:
            new_entry["email"] = new_user_fields["email"]

    # do fullname
    if new_user_fields.get("fullname", None):
        new_entry["fullname"] = new_user_fields["fullname"]
    else:
        return {"result": "fail", "failure_reason": "no_fullname"}

    # do roles - add to list
    if new_user_fields.get("roles", None):
        for item in new_user_fields["roles"]:
            new_entry["roles"].append(item)

    # do username
    if new_user_fields.get("username", None):
        user_record = get_user_record_by_userid(userid=new_user_fields["username"])
        if user_record:
            return {"result": "fail", "failure_reason": "user_exists"}
        else:
            new_entry["userid"] = new_user_fields["username"]

    else:
        result = {"result": "fail", "failure_reason": "no_username"}

    # do password and hasish
    if new_user_fields.get("password", None):
        new_salt = gen_salt()
        new_entry["auth_salt"] = new_salt
        generated_pass = secure_hash_password(plain_text=new_user_fields["password"], pass_salt=new_salt)
        new_entry["auth_password"] = generated_pass

    else:
        result = {"result": "fail", "failure_reason": "no_password"}

    # create new user returns True
    create_new_user = make_new_user(use_fields=new_entry)
    if not create_new_user:
        return {"result": "fail", "failure_reason": "fail_user_creation"}
    else:
        return {"result": "success"}

    return result


def update_user_fields(new_user_fields="", userid=None):
    """will take new user fields and extract them from a HTTP header.
    Then sees if user exsits in mongo db. Extracts specified fields so to be secure.
    Then updates fields.

    Userid gets passed from auth headers which are string by def and get converted
    to json. Fixes a bug were quoutes were addded to a username"""

    # set default

    result = {"result": "fail", "failure_reason": "unspecified_error_in_user_edit"}

    if not new_user_fields:
        return {"result": "fail", "failure_reason": "insufficient_info_supplied"}

    if not userid:
        return {"result": "fail", "failure_reason": "insufficient_info_supplied"}

    # try loading as JSON
    try:
        new_user_fields = json.loads(new_user_fields)
    except:
        return result

    try:
        userid = json.loads(userid)
    except:
        return result

    # do username
    user_record = get_user_record_by_userid(userid=userid)
    logging.debug(user_record)
    if not user_record:
        return {"result": "fail", "failure_reason": "invalid_user"}

    update_fields = {}  # set default

    # do roles - add to list
    # can not use .get on key because user may have removed all their privs
    # resulting in empty list of user roles so if will evalulate as false
    # to get around this, we set to default empty list and then add any roles which may be there
    update_fields["roles"] = []
    if new_user_fields.get("roles", None):
        for item in new_user_fields["roles"]:
            update_fields["roles"].append(item)

    # do email
    if new_user_fields.get("email", None):
        if not "@" in new_user_fields["email"]:
            return {"result": "fail", "failure_reason": "invalid_email"}
        else:
            update_fields["email"] = new_user_fields["email"]

    # do fullname
    if new_user_fields.get("fullname", None):
        update_fields["fullname"] = new_user_fields["fullname"]

    if update_user_record(userid=userid, update_fields=update_fields):
        return {"result": "success"}

    return result


def get_user_and_userid_records():
    """gets username and id records from a function
    will return an error in "result" key dict or return actual results
    """
    result = {"result": "fail", "failure_reason": "unspecified_error_getting_all_records"}

    allresults = get_all_user_and_fullname()
    result = {"result": "success", "return_data": allresults}

    return result


def delete_user_by_id(userid=""):
    """gets username and id records from a function
    will return an error in "result" key dict or return actual results
    """
    result = {"result": "fail", "failure_reason": "unspecified_error_deleting_usr_record"}

    if not userid:
        return result

    userid = json.loads(userid)  # removes wrapped quotes as passed from http header

    # cannot delete admin account
    if userid == "admin":
        result = {"result": "fail", "failure_reason": "system_user_cannot_be_deleted"}
        return result

    delete_result = delete_user_record(userid=userid)

    if delete_result:
        result = {"result": "success", "return_data": "deleted"}
        return result

    return result


# uncomment for local debug (mongo db must be exposed)
# MONGO_URL="mongodb://localhost:27017"
# print(authenticate_user(http_auth_header={'username': 'admin', 'password': 'blah', 'token': 'dummytoken'}))
