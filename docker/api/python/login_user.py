from auth_handle.defaults import gen_default_submission_login
import time
import getpass
import pprint as pp
from pymongo_mod.pymongo_get_user_record import *
from pymongo_mod.pymongo_functions import *
from auth_handle.auth import *


def ask_yes_no_prompt(question_str=""):
    """prompts usrr for yes/no. Repeats prompt if invalid resp"""
    answered = False

    while answered is False:
        reply = input(question_str)
        if reply.lower() == "y":
            return True
        if reply.lower() == "n":
            return False


new_sumission = gen_default_submission_login()

existing_token = ask_yes_no_prompt(question_str="Have existing token? [y/n]: ")

if existing_token:
    new_sumission["token"] = input("Enter new token")

if not existing_token:
    username = input("Enter username: ")
    password = getpass.getpass("Enter your password: ")

    new_sumission["username"] = username
    new_sumission["password"] = password


new_sumission["time"] = time.time()

pp.pprint(new_sumission)


user_record = get_user_record_by_userid(userid=new_sumission["username"])
if not user_record:
    print("User not found")
else:
    try_pass = secure_hash_password(plain_text=new_sumission["password"], pass_salt=user_record["auth_salt"])
    if try_pass == user_record["auth_password"]:
        print("Logged in!")
    # elif new_sumission["token"]==user_record["auth_token_generated"]:
    #    print("Logged in!")
    else:
        print("Bad Login Info")
