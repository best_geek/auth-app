"""
Version: 1.5
Author: Nathan Webb
Date Created: 20210709
flask API for recipeez
"""
import argparse
from datetime import datetime
import flask
from flask import jsonify
from flask import request
from flask_cors import CORS
from api_endpoints import *
import json
from pymongo_mod.pymongo_functions import *
from pymongo import MongoClient
from pymongo.errors import *
from pymongo_config import *
from bson.objectid import ObjectId
import time
import sys


# configure flask
app = flask.Flask(__name__)
LISTEN_PORT = 5000
CORS(app)  # remove when not doing cross origin


# default uri call
@app.route("/", methods=["GET"])
def base():

    status = {"name": "auth_app", "state": "operational", "version": "1.0"}

    return jsonify(status)


# status url call
@app.route("/status", methods=["GET"])
def status():
    return_result = {"result": "mongodb_up"}

    if checkconnect is False:
        return_result = {"result": "mongodb_down"}

    return return_result


@app.route("/auth/login", methods=["POST", "GET"])
def auth_login():

    authentication = request.headers.get("auth")

    result = authenticate_user(http_auth_header=authentication)
    return jsonify(result)


@app.route("/auth/logout_cookie", methods=["GET"])
def auth_logout_cookie():
    """
    we can remove the last sent token sent to invalidate it, even if the browser
    JS doesn't clear that token, it will be no longer valid anyway

    attempted authentication will have generated a new token to pass back to the application
    We intercept it and remove so unused tokens don't build up. Then return if we cleared the token
    or not

    note headers come in as type string, if you wish to manipulate change to another type *AFTER*
    running the authentication
    """
    authentication = request.headers.get("auth")

    result = authenticate_user(http_auth_header=authentication)
    if result["result"] == "success":
        # ["auth_data"]["token"] contains the new token the authentication handler would have submitted back
        # auth_data also contains the username from previous authentication
        signout_result = signout_specific_token(userid=result["auth_data"]["username"], token=result["auth_data"]["token"])
        logging.info(f"Removed token for user: {result['auth_data']['username']} token: {result['auth_data']['token']}")
        return jsonify(signout_result)

    return jsonify({"result": "fail", "failure_reason": "submitted_token_not_valid"})


@app.route("/user/details/me", methods=["GET"])
def user_details_me():
    """
    gets user details for current user which is why URI has /me
    Will take the user's ID submitted in the authentication as which userID to lookup. We authenticate the user
    first so that we can verify the requester is that actually user. If they weren't, the tokens wouldn't match

    note headers come in as type string, if you wish to manipulate change to another type *AFTER*
    running the authentication
    """
    authentication = request.headers.get("auth")

    auth_result = authenticate_user(http_auth_header=authentication)
    if auth_result["result"] == "success":

        user_record = get_safe_user_info_by_record(userid=json.loads(authentication)["username"])

        return_result = {}
        return_result["return_data"] = {}
        return_result["return_data"] = user_record
        return_result["result"] = "success"
        return_result = {**return_result, **auth_result}  # add our authentication fields

        return jsonify(return_result)

    return jsonify({"result": "fail", "failure_reason": "unauthenticated"})


@app.route("/user/details/get", methods=["GET"])
def user_details_get():
    """
    gets user details by id
    Will take the user's ID submitted in the authentication as which userID to lookup. We authenticate the user
    first so that we can verify the requester is that actually user. If they weren't, the tokens wouldn't match

    note headers come in as type string, if you wish to manipulate change to another type *AFTER*
    running the authentication
    """
    authentication = request.headers.get("auth")
    requested_user = request.headers.get("userid_val")

    auth_result = authenticate_user(http_auth_header=authentication)

    return_result = {}  # set default
    if auth_result["result"] == "success":

        # if we're changing a user password which wasn't submitted as part of token
        # make sure user has user_admin role
        if json.loads(authentication)["username"] != requested_user:

            # user has role returns true or false
            if not user_has_role(userid=json.loads(authentication)["username"], query_role="user_admin"):
                return_result["failure_reason"] = "user_insufficient_privelleges"
                return_result["result"] = "fail"
            else:
                usr_lookup_result = get_safe_user_info_by_record(userid=requested_user)

                # change result includes a failure reason to return as well
                return_result = {}
                return_result["return_data"] = usr_lookup_result
                return_result["result"] = "success"
                return_result = {**return_result, **auth_result}

        else:
            usr_lookup_result = get_safe_user_info_by_record(userid=requested_user)

            # change result includes a failure reason to return as well
            return_result = {}
            return_result["return_data"] = usr_lookup_result
            return_result["result"] = "success"
            return_result = {**return_result, **auth_result}

        return_result = {**return_result, **auth_result}  # add our authentication fields

        return jsonify(return_result)

    return jsonify({"result": "fail", "failure_reason": "unauthenticated"})


@app.route("/user/change/password", methods=["GET"])
def user_change_password():
    """
    Takes password by user authentication. Will authenticate user first.
    If the userid sent does not match the one sent in the authentication cookie,
    then they must have the user_admin role
    """
    authentication = request.headers.get("auth")
    requested_user_for_change = request.headers.get("userid_val")
    submitted_password_change = request.headers.get("pass_val")

    auth_result = authenticate_user(http_auth_header=authentication)

    logging.info(f"password change requested for {requested_user_for_change}")

    return_result = {}
    return_result = {**return_result, **auth_result}  # add our authentication fields

    if auth_result["result"] == "success":

        # if we're changing a user password which wasn't submitted as part of token
        # make sure user has user_admin role
        if json.loads(authentication)["username"] != requested_user_for_change:

            # user has role returns true or false
            if not user_has_role(userid=json.loads(authentication)["username"], query_role="user_admin"):
                return_result["failure_reason"] = "user_insufficient_privelleges"
                return_result["result"] = "fail"
            else:
                change_result = change_password_by_id(userid=requested_user_for_change, plain_text_pass=submitted_password_change)
                # change result includes a failure reason to return as well
                return_result = {**return_result, **change_result}

        # the same user with the auth token == username to change. Therefore changing their own
        # user record in mongodb
        if json.loads(authentication)["username"] == requested_user_for_change:
            change_result = change_password_by_id(userid=requested_user_for_change, plain_text_pass=submitted_password_change)
            # change result includes a failure reason to return as well
            return_result = {**return_result, **change_result}

    return jsonify(return_result)


@app.route("/user/create/", methods=["GET"])
def user_create():
    """
    Populates user entry. Will only run if has role user_admin
    """
    authentication = request.headers.get("auth")
    creation_fields = request.headers.get("create_fields")

    auth_result = authenticate_user(http_auth_header=authentication)

    return_result = {}
    return_result = {**return_result, **auth_result}  # add our authentication fields

    if auth_result["result"] == "success":

        # user has role returns true or false
        if not user_has_role(userid=json.loads(authentication)["username"], query_role="user_admin"):
            return_result["failure_reason"] = "user_insufficient_privelleges"
            return_result["result"] = "fail"
        else:
            new_user_result = create_new_user(new_user_fields=creation_fields)
            return_result = {**return_result, **new_user_result}

    return jsonify(return_result)


@app.route("/user/edit/", methods=["GET"])
def user_edit():
    """
    Populates user entry fields Will only run if has role user_admin
    """
    authentication = request.headers.get("auth")
    update_fields = request.headers.get("update_fields")
    user_id_to_update = request.headers.get("userid_val")

    logging.debug(f"user id to update fields: {user_id_to_update}")

    auth_result = authenticate_user(http_auth_header=authentication)

    return_result = {}
    return_result = {**return_result, **auth_result}  # add our authentication fields

    if auth_result["result"] == "success":

        # error catching for required data
        if not user_id_to_update or not update_fields:
            result = {"result": "fail", "failure_reason": "API_called_without_required_info"}
            return_result = {**auth_result, **result}
            return jsonify(return_result)

        # user has role returns true or false
        if not user_has_role(userid=json.loads(authentication)["username"], query_role="user_admin"):
            return_result["failure_reason"] = "user_insufficient_privelleges"
            return_result["result"] = "fail"
        else:
            update_usr_result = update_user_fields(new_user_fields=update_fields, userid=user_id_to_update)
            logging.debug(update_usr_result)
            return_result = {**return_result, **update_usr_result}

    return jsonify(return_result)


@app.route("/user/list/all", methods=["GET"])
def user_list_all():
    """
    Populates user entry fields Will only run if has role user_admin
    """

    authentication = request.headers.get("auth")
    auth_result = authenticate_user(http_auth_header=authentication)

    return_result = {}
    return_result = {**return_result, **auth_result}  # add our authentication fields

    if auth_result["result"] == "success":

        # user has role returns true or false
        if not user_has_role(userid=json.loads(authentication)["username"], query_role="user_admin"):
            return_result["failure_reason"] = "user_insufficient_privelleges"
            return_result["result"] = "fail"
        else:
            allresults = get_user_and_userid_records()
            return_result = {**return_result, **allresults}

    return jsonify(return_result)


@app.route("/user/delete/", methods=["GET"])
def user_delete():
    """
    Deletes a record by user id. Users can delete their own records
    Users can only delete other records if they have the user_admin rol
    """
    authentication = request.headers.get("auth")
    user_id_to_delete = request.headers.get("userid_val")

    auth_result = authenticate_user(http_auth_header=authentication)

    return_result = {}
    return_result = {**return_result, **auth_result}  # add our authentication fields

    if auth_result["result"] == "success":

        # error catching for required data
        if not user_id_to_delete:
            result = {"result": "fail", "failure_reason": "API_called_without_required_info"}
            return_result = {**auth_result, **result}
            return jsonify(return_result)

        # if we're changing a user password which wasn't submitted as part of token
        # make sure user has user_admin role
        if json.loads(authentication)["username"] != user_id_to_delete:

            # user has role returns true or false
            if not user_has_role(userid=json.loads(authentication)["username"], query_role="user_admin"):
                return_result["failure_reason"] = "user_insufficient_privelleges"
                return_result["result"] = "fail"
            else:
                delete_result = delete_user_by_id(userid=user_id_to_delete)
                # change result includes a failure reason to return as well
                return_result = {**return_result, **delete_result}

        # the same user with the auth token == username to change. Therefore changing their own
        # user record in mongodb
        if json.loads(authentication)["username"] == user_id_to_delete:
            # change result includes a failure reason to return as well
            delete_result = delete_user_by_id(userid=user_id_to_delete)
            return_result = {**return_result, **delete_result}

    return jsonify(return_result)

#turn off debuging so users can't input a debug key if things fail
app.config.update(
    TESTING=False,
    SECRET_KEY='0badab321f1854fd2c7c11948dc5a4d9abc181934da901e02b0767d7f5355ef2'
)

app.run(host="0.0.0.0", port=int(LISTEN_PORT), debug=False)
