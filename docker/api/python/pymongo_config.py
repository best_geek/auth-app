MONGO_DEST = "authapp_mongodb"
MONGO_PORT = "27017"
AUTH_DBNAME = "authentication"
AUTH_COLLECT = ["users"]
MONGO_URL = f"mongodb://{MONGO_DEST}:{MONGO_PORT}"
