from pymongo_mod.pymongo_functions import *
from pymongo.errors import *
from pymongo import MongoClient
import logging
import sys
from auth_handle.auth import *
from auth_handle.defaults import *
from pymongo_config import *

logging.basicConfig(format="[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s", datefmt="%m/%d/%Y_%I:%M:%S")
logging.root.setLevel(logging.DEBUG)

DEF_ADMIN_PASSWORD = "changeme"
DEF_ADMIN_USERID = "admin"


logging.info(f"Connecting to {MONGO_URL}")
logging.info(f"Using collections: {AUTH_COLLECT}")

# uncomment for local debug (mongo db must be exposed)
# MONGO_URL="mongodb://localhost:27017"

# returns true/false
upstatus = checkconnect(MONGO_URL)
if upstatus is False:
    logging.error("MongoDB connection is down. Exiting")
    sys.exit(1)
else:
    logging.info("MongoDB connection established")

# create a client handler now we're up and running. Easy to reference
client = connectdb(MONGO_URL)

# get all databases so we have refence var for multiple purposes
ALL_DBS = getalldb(client)
logging.info(f"Available databases: {ALL_DBS}")

# create of not existing
if AUTH_DBNAME in ALL_DBS:
    logging.info(f"Detected exisitng database: {AUTH_DBNAME}")
    DB_EXIST = True
else:
    DB_EXIST = False
    logging.warning(f"No database existed. One will be created")
db = client[AUTH_DBNAME]

# create collection name if db did not exist
for item in AUTH_COLLECT:
    COL = db[item]


# use defaults.py to look for a base datastructure
new_entry = gen_default_usr_entry()
new_salt = gen_salt()
pass_str = secure_hash_password(plain_text=DEF_ADMIN_PASSWORD, pass_salt=new_salt)

# update default entry json
new_entry["userid"] = DEF_ADMIN_USERID
new_entry["fullname"] = "Administrator"
new_entry["auth_salt"] = new_salt
new_entry["auth_password"] = pass_str
new_entry["roles"].append("user_admin")


# find out if it should be replaced or insert new records
query_admin = {"userid": DEF_ADMIN_USERID}
query_admin_exists = COL.find(query_admin)
query_admin_exists_records = 0
for doc in query_admin_exists:
    query_admin_exists_records += 1

if query_admin_exists_records:
    logging.info(f"Admin user existed. Will not recreate")
    sys.exit(0)
else:
    inserted = COL.insert_one(new_entry)


logging.info(f"admin user now set up. Password set to {DEF_ADMIN_PASSWORD}")

