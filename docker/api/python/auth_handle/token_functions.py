import logging
from re import sub
import time
import secrets

logging.basicConfig(format='[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s',datefmt='%m/%d/%Y_%I:%M:%S')
logging.root.setLevel(logging.DEBUG)

TOKEN_RENEW_MINS=120

def see_if_token_valid(pymongo_record={},submitted_token=""): 
    """
    gets fields from an existing pymongo document 
    sees if token is still valid and authenticates it

    valid_until should be an epoch number

    if we can get a field with the keyvalue then we must know the token 
    matches

    returns valid or expired or false
    """
    

    #explicit typing to dict to fix bug where might be nonetype
    try:
        auth_token=dict(pymongo_record["tokens"]).get(submitted_token,None)
    except (AttributeError or TypeError): #probably isn't a dict
        return 


    if not auth_token:
        return

    #get valid until 
    valid_until=pymongo_record["tokens"][submitted_token].get("auth_token_valid_to",None)


    #only if token still valid
    if int(time.time()) > valid_until:
        return "expired"
    if int(time.time()) < valid_until:
        return "valid"


def return_record_popped_token(pymongo_record={},submitted_token=""): 
    """
    gets fields from an existing pymongo document 
    sees if token is still valid and authenticates it

    valid_until should be an epoch number

    returns true or false
    """

    auth_token=pymongo_record["tokens"].get(submitted_token,None)
    userid=pymongo_record["userid"]

    if not auth_token:
        logging.warning(f"requested token ({submitted_token}) removal for user: {userid}  but token did not exist")
        return

    #delete and return 
    del pymongo_record["tokens"][submitted_token]
    return pymongo_record["tokens"]



def gen_new_token(): 
    """
    returns a new token and when it's valid until

    uses fields specifed in mongo DB so we can easily update records

    structure is:
    tokens={}
    tokens{'someunique_token':{}}
    tokens{'someunique_token':{
        "auth_token_generated": epoch time,
        "auth_token_valid_to", epoch time
    }}
    """

    generated_token=secrets.token_hex(40)
    new_token_return={}
    new_token_return[generated_token]={}
    new_token_return[generated_token]["auth_token_generated"]=int(time.time())

    #new valid to time
    #takes the curent time and adds epoch renew minutes
    new_valid_to=int(time.time())+(TOKEN_RENEW_MINS*60)
    new_token_return[generated_token]["auth_token_valid_to"]=new_valid_to

    return new_token_return






