import secrets
import logging
from hashlib import sha3_512
from time import perf_counter_ns

logging.basicConfig(format='[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s',datefmt='%m/%d/%Y_%I:%M:%S')
logging.root.setLevel(logging.DEBUG)


def gen_salt():
    """Generates a cryptographically secure random salt for passwords"""
    logging.info("Generating salt for password")
    generated_salt=secrets.token_hex(20)
    logging.info("Generated salt successfully")
    return generated_salt

def secure_hash_password(plain_text="",pass_salt=""):
    """Securely generates a password with plain text and a salt"""

    #record how long hashing takes
    hash_func_start=perf_counter_ns()

    if not plain_text:
        logging.error("No plaintext password supplied")
        return
    if not pass_salt:
        logging.error("No salt supplied")
        return

    if len(plain_text) < 8:
        logging.warning("Supplied plain text password less than 8 in len. Not secure.")

    #goes through and securely generates a hashed password
    
    #takes each char, multiplies by it's position and generates a hash
    #added that chars hash to a base hash which will get hashed later
    base_hash_str=""
    for i in range(0,len(plain_text)):
        char=plain_text[i]
        update_str=i*plain_text
        hashed_portion=sha3_512(update_str.encode('utf-8'))
        base_hash_str+=hashed_portion.hexdigest()

    #generate final hash
    base_hash_str+=pass_salt
    final_hash=sha3_512(base_hash_str.encode('utf-8'))
    final_hash_str=final_hash.hexdigest()

    #capture end time
    hash_funct_stop=perf_counter_ns()
    elaspsed_funct_time=hash_funct_stop-hash_func_start
    logging.debug(f"Took {elaspsed_funct_time} nanoseconds for hash generation")

    return final_hash_str


def gen_new_token():
    """Generate a new token"""
    return secrets.token_hex(40)

