def gen_default_usr_entry():
    """Generates a default dictionary for user entry in the mongoDB"""
    entry={}
    entry["userid"]=None
    entry["fullname"]=None
    entry["email"]=None
    entry["auth_salt"]=None
    entry["auth_password"]=None
    entry["tokens"]={}
    entry["last_login"]=None
    entry["auth_OTP_str"]=None
    entry["auth_OPT_code"]=None
    entry["roles"]=[]

    return entry


def gen_default_submission_login():
    """Generates a default dictionary for initial authentication"""
    submission={}
    submission["token"]=None
    submission["username"]=None
    submission["password"]=None
    submission["time"]=None
    return submission