from pymongo import MongoClient
from pymongo.errors import *


def checkconnect(mongourl, timeoutMS=2):
    client = MongoClient(mongourl,serverSelectionTimeoutMS=timeoutMS)
    try:
        # The ismaster command is cheap and does not require auth.
        client.admin.command('ismaster')
    except ConnectionFailure:
        return False


def connectdb (mongourl):
    client = MongoClient(mongourl)
    return client


def getalldb(mongoclient):
    return mongoclient.list_database_names()

def getcollection (db,collectioname):
    return db[collectioname]