from pymongo_mod.pymongo_functions import *
#from pymongo_functions import *
from pymongo.errors import *
from pymongo import MongoClient
import logging
import sys
import os
import time

#import modules from parent directory
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from pymongo_config import * #note - unmetioned variables are pulled from here containing config

logging.basicConfig(format='[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s',datefmt='%m/%d/%Y_%I:%M:%S')
logging.root.setLevel(logging.DEBUG)


logging.debug(f"Added parent functions to path from {parentdir}")


logging.info(f"Connecting to {MONGO_URL}")
logging.info(f"Using collection: {AUTH_COLLECT}")



def get_user_record_by_userid(userid=""): 
    """
    gets a user record by userid
    Returns true or false depending if any records found.
    Imported config from parent dir

    """

    logging.debug(f"geting user by id: {userid}")

    #time.sleep(0.5)

    #returns true/false
    upstatus=checkconnect(MONGO_URL)
    if upstatus is False:
        logging.error("MongoDB connection is down. Exiting")
        return
    else:
        logging.info("MongoDB connection established")

    #create a client handler now we're up and running. Easy to reference
    client=connectdb(MONGO_URL)

    #switch to DB
    db = client[AUTH_DBNAME]

    #create collection name if db did not exist
    for item in AUTH_COLLECT:
        COL=db[item]


    #find out if records exist
    query_user={"userid": userid}
    query_user_exists = COL.find(query_user)
    query_user_exists_records=0
    any_found_doc=[]
    for doc in query_user_exists:
        any_found_doc.append(doc)
        query_user_exists_records+=1


    #if records exist, return them as JSON 
    if query_user_exists_records:
        return any_found_doc[0] #run findone() so only one record in list

    else:
        logging.debug(f"User '{userid}' not found in mongodb")
        return False



def get_all_user_and_fullname(): 
    """
    returns all user records and their userid and fullname in the authentication user collection
    we only limit return fields of username and id, so not to pass back every user field per record
    
    returns type list. Will return empty list if no results
    """

    logging.debug(f"getting all user information")

    all_user_results=[] #empty default

    #returns true/false
    upstatus=checkconnect(MONGO_URL)
    if upstatus is False:
        logging.error("MongoDB connection is down. Exiting")
        return
    else:
        logging.info("MongoDB connection established")

    #create a client handler now we're up and running. Easy to reference
    client=connectdb(MONGO_URL)

    #switch to DB
    db = client[AUTH_DBNAME]

    #create collection name if db did not exist
    for item in AUTH_COLLECT:
        COL=db[item]


    results=COL.find({})

    for doc in results:
        new_entry={}
        new_entry["fullname"]=doc.get("fullname",None)
        new_entry["userid"]=doc.get("userid", None)
        all_user_results.append(new_entry)
  
    return all_user_results

def make_new_user(use_fields={}): 
    """
    creates a new document 
    """


    #time.sleep(0.5)

    #returns true/false
    upstatus=checkconnect(MONGO_URL)
    if upstatus is False:
        logging.error("MongoDB connection is down. Exiting")
        return
    else:
        logging.info("MongoDB connection established")

    #create a client handler now we're up and running. Easy to reference
    client=connectdb(MONGO_URL)

    #switch to DB
    db = client[AUTH_DBNAME]

    #create collection name if db did not exist
    for item in AUTH_COLLECT:
        COL=db[item]

    
    inserted=COL.insert_one(use_fields)
    logging.info(f"Create new user: {use_fields['userid']}")
    return True





def user_has_role(userid="",query_role=""): 
    """
    gets a user record by userid
    Returns true or false depending if any records found.
    Imported config from parent dir

    """

    logging.debug(f"eval if {userid} has role {query_role}")


    user_record=get_user_record_by_userid(userid=userid)
    if user_record:
        if query_role in user_record["roles"]:
            return True
        else:logging.warning(f"'{userid}' requested action which required '{query_role}' but does not have role")


    

    return False


def delete_user_record(userid=""): 
    """
    removes a user record by userid. Will remove all occurances where a user
    id exists matching
    Returns true or false depending if any records found.
    Imported config from parent dir

    """

    logging.debug(f"requested removal of: {userid}")

    if not userid:
        return

    #returns true/false
    upstatus=checkconnect(MONGO_URL)
    if upstatus is False:
        logging.error("MongoDB connection is down. Exiting")
        return
    else:
        logging.info("MongoDB connection established")

    #create a client handler now we're up and running. Easy to reference
    client=connectdb(MONGO_URL)

    #switch to DB
    db = client[AUTH_DBNAME]

    #create collection name if db did not exist
    for item in AUTH_COLLECT:
        COL=db[item]


    user_query={"userid":userid}


    deleted=COL.delete_many(user_query)


    if deleted.deleted_count > 0:
        return True
    else:
        return

#uncomment for local debug (mongo db must be exposed)
#MONGO_URL="mongodb://localhost:27017"

#print(get_user_record_by_userid(userid="admin"))
#print(get_user_record_by_userid(userid="pooop"))