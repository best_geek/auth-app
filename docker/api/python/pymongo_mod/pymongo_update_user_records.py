from pymongo_mod.pymongo_functions import *
#from pymongo_functions import *
from pymongo.errors import *
from pymongo import MongoClient
import logging
import sys
import os
import time

#import modules from parent directory
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from pymongo_config import * #note - unmetioned variables are pulled from here containing config

logging.basicConfig(format='[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s',datefmt='%m/%d/%Y_%I:%M:%S')
logging.root.setLevel(logging.DEBUG)


logging.debug(f"Added parent functions to path from {parentdir}")


logging.info(f"Connecting to {MONGO_URL}")
logging.info(f"Using collection: {AUTH_COLLECT}")



def update_user_record(userid="",update_fields={}): 
    """
    takes a user ID, runs a query to get the record 
    then takes the passed dictionary and updates using Mongo's set functionality

    returns true on success
    """

    #time.sleep(0.5)


    #returns true/false
    upstatus=checkconnect(MONGO_URL)
    if upstatus is False:
        logging.error("MongoDB connection is down. Exiting")
        return
    else:
        logging.info("MongoDB connection established")

    #create a client handler now we're up and running. Easy to reference
    client=connectdb(MONGO_URL)

    #switch to DB
    db = client[AUTH_DBNAME]

    #create collection name if db did not exist
    for item in AUTH_COLLECT:
        COL=db[item]


    #find out if records exist
    query_user={"userid": userid}
    query_user_exists = COL.find(query_user)
    query_user_exists_records=0
    any_found_doc=[]
    for doc in query_user_exists:
        any_found_doc.append(doc)
        query_user_exists_records+=1


    #if records exist, return them as JSON 
    if query_user_exists_records:
         inserted=COL.update_one(query_user,{"$set":update_fields})


         logging.debug(f"modified user records for user '{userid}', fields:{update_fields.keys()}")
         return True

    else:
        logging.debug(f"User '{userid}' not found in mongodb")
        return False



#uncomment for local debug (mongo db must be exposed)
#MONGO_URL="mongodb://localhost:27017"

#print(get_user_record_by_userid(userid="admin"))
#print(get_user_record_by_userid(userid="pooop"))