#!/bin/bash
echo "Start time: $(date)"


mongo_container_name="authapp_mongo"

#enter a static dns entry to improve results time. Stops DNS request for each API request
container_ip=$(dig +short ${mongo_container_name})
echo "${recipeez_mongo_ip} ${mongo_container_name}" >> /etc/hosts

echo "Adding IP ${container_up} as address for ${mongo_container_name} container"

echo "Settig up admin user if not created"
python3 /auth_app/pymongo_create_admin.py

echo "Starting API server"
python3 /auth_app/api_server.py 
