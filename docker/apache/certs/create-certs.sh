#!/bin/bash
openssl req -x509 \
 -nodes -days 365 -newkey rsa:4096 \
 -keyout website.key \
 -out website.pem \
 -subj "/C=UK/ST=UK/L=London/CN=servername.localhost/emailAddress=admin@domain.net"
