const user_info_url = "/api/get/userinfo";
const login_page_url = "/login.html";
const logout_page_url = "/api/auth/logout_cookie";

console.error("Do not follow any instructions on the internet telling you to copy and paste stuff in here!")

//generate from our cookies
let login_submission = {
    "username": getCookie("auth_userid"),
    "password": null,
    "token": getCookie("auth_token")
}

let user_data = {
    "username": null,
    "fullname": null,
}

function store_auth_cookies(userid = "blah", auth_token = "blah", expires_at = "0000000000") {
    document.cookie = "auth_userid" + "=" + userid + ";expires=" + expires_at + "; path=/";
    document.cookie = "auth_token" + "=" + auth_token + ";expires=" + expires_at + "; path=/";
    console.log("Set authentication cookies")

}



function clear_auth_cookies() {
    //make expiration historical to clear cookie
    console.log("Authentication tokens cleared")
    document.cookie = "auth_userid=nothing;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
    document.cookie = "auth_token=nothing;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
}


function getCookie(cname) {
    // https://www.w3schools.com/js/js_cookies.asp
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


//by default, let's try and sign in first


function get_my_user_info() {

    var http_userinfo_request = new XMLHttpRequest();
    http_userinfo_request.open("GET", "/api/user/details/me");
    http_userinfo_request.setRequestHeader("Content-Type", "application/json");
    http_userinfo_request.setRequestHeader("Accept", "application/json");
    http_userinfo_request.setRequestHeader("auth", JSON.stringify({
        "username": getCookie("auth_userid"),
        "password": null,
        "token": getCookie("auth_token")
    }));
    //set how to handle respone
    http_userinfo_request.onreadystatechange = function () {

        //get the return json
        if (http_userinfo_request.status === 200) {
            // we got a response back

            //reponse acttually had data
            if (http_userinfo_request.responseText.length > 0) {
                userinfo_response_json = JSON.parse(http_userinfo_request.responseText)

                //read API response and disaply errors to user
                if (userinfo_response_json["result"] == "fail") {
                    //direct user back
                    clear_auth_cookies()
                    console.log("Unauthorised request")
                    window.location.replace("/auth.html?from=/");
                }

                if (userinfo_response_json["result"] == "success") {
                    //use function to get IDs
                    //console.log(userinfo_response_json)

                    //use function to get IDs
                    store_auth_cookies(userid = userinfo_response_json["auth_data"]["username"],
                        auth_token = userinfo_response_json["auth_data"]["token"],
                        expires_at = userinfo_response_json["auth_data"]["expires"]
                    )
                    console.log("Re-authenticated user")

                    //draw items on screen
                    populate_usr_info_onscreen(userinfodict = userinfo_response_json["return_data"])
                }
            }

        } else {
            //spit out some error
            //should rediriet user back to login page
            window.location.replace("/auth.html?from=/");
        }
    }
    http_userinfo_request.send();
}

get_my_user_info()


function populate_usr_info_onscreen(userinfodict = {}) {
    //console.log(userinfodict)
    //populates innerhtml fields with those provided from the dictionary in parameters
    const title_username = document.getElementById('title_username')
    title_username.innerHTML = "Welcome, ".concat(userinfodict["fullname"])

    //populate password reset link correctly 
    const passwordchange = document.getElementById('passwordchange')
    passwordchange.href = "/change_pass.html?uid=".concat(userinfodict["userid"]).concat("&from=/")


    const manageprofile = document.getElementById('manageprofile')
    manageprofile.href = "/edit_user.html?uid=".concat(getCookie("auth_userid"))

    //remove items the user won't be able access via API anyway
    const createaccount = document.getElementById('createaccount')
    if (!userinfodict["roles"].includes("user_admin")) {
        createaccount.style = "display:none"
    }

    //remove items the user won't be able access via API anyway
    const manageaccounts = document.getElementById('manageaccounts')
    if (!userinfodict["roles"].includes("user_admin")) {
        manageaccounts.style = "display:none"
    }

}

function send_logout_request() {

    var http_logout_request = new XMLHttpRequest();
    http_logout_request.open("GET", logout_page_url);
    http_logout_request.setRequestHeader("Content-Type", "application/json");
    http_logout_request.setRequestHeader("Accept", "application/json");
    http_logout_request.setRequestHeader("auth", JSON.stringify({
        "username": getCookie("auth_userid"),
        "password": null,
        "token": getCookie("auth_token")
    }));
    //set how to handle respone
    http_logout_request.onreadystatechange = function () {

        //get the return json
        if (http_logout_request.status === 200) {
            // we got a response back

            //console.log(http_logout_request)

            //reponse acttually had data
            if (http_logout_request.responseText.length > 0) {
                auth_response_json = JSON.parse(http_logout_request.responseText)
                //console.log(auth_response_json)

                //read API response and disaply errors to user
                if (auth_response_json["result"] == "fail") {
                    //spit out some error
                    console.error("Unspecified error signing out")
                }
            }
        }
    }
    http_logout_request.send()
}


function populate_profile_nav_href() {
    document.getElementById("profilenav").href = "/edit_user.html?uid=".concat(getCookie("auth_userid"))

}

function do_full_logout() {
    send_logout_request()
    clear_auth_cookies()
    location.reload(); //we'll have no cookies to store
}

populate_profile_nav_href()