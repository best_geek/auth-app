const login_url = "/api/auth/login";
const edit_url = "/api/user/edit/"
const logout_page_url = "/api/auth/logout_cookie";


console.log("Loaded authentication JS")
console.error("Do not follow any instructions on the internet telling you to copy and paste stuff in here!")



function store_auth_cookies(userid = "blah", auth_token = "blah", expires_at = "0000000000") {
    document.cookie = "auth_userid" + "=" + userid + ";expires=" + expires_at + "; path=/";
    document.cookie = "auth_token" + "=" + auth_token + ";expires=" + expires_at + "; path=/";
    //console.log("Set authentication cookies")
}

function clear_auth_cookies() {
    //make expiration historical to clear cookie
    console.log("Authentication tokens cleared")
    document.cookie = "auth_userid=nothing;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
    document.cookie = "auth_token=nothing;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";

}


function getCookie(cname) {
    // https://www.w3schools.com/js/js_cookies.asp
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}



function get_user_info() {

    var http_userinfo_request = new XMLHttpRequest();
    http_userinfo_request.open("GET", "/api/user/details/get");
    http_userinfo_request.setRequestHeader("Content-Type", "application/json");
    http_userinfo_request.setRequestHeader("Accept", "application/json");
    http_userinfo_request.setRequestHeader("auth", JSON.stringify({
        "username": getCookie("auth_userid"),
        "password": null,
        "token": getCookie("auth_token")
    }));

    http_userinfo_request.setRequestHeader("userid_val", user_to_change)


    //set how to handle respone
    http_userinfo_request.onreadystatechange = function () {

        //get the return json
        if (http_userinfo_request.status === 200) {
            // we got a response back

            //reponse acttually had data
            if (http_userinfo_request.responseText.length > 0) {
                userinfo_response_json = JSON.parse(http_userinfo_request.responseText)

                //read API response and disaply errors to user
                if (userinfo_response_json["result"] == "fail") {
                    //direct user back
                    clear_auth_cookies()
                    console.log("Unauthorised request")
                    window.location.replace("/auth.html?from=/");
                }

                if (userinfo_response_json["result"]["auth_data"] !== null) {
                    //use function to get IDs

                    //use function to get IDs
                    store_auth_cookies(userid = userinfo_response_json["auth_data"]["username"],
                        auth_token = userinfo_response_json["auth_data"]["token"],
                        expires_at = userinfo_response_json["auth_data"]["expires"]
                    )
                    console.log("Re-authenticated user")

                    //draw items on screen
                    populate_usr_info_onscreen(userinfodict = userinfo_response_json["return_data"])
                }
            }

        } else {
            //spit out some error
            //should rediriet user back to login page
            window.location.replace("/auth.html?from=/");
        }
    }
    http_userinfo_request.send();
}

function populate_usr_info_onscreen(userinfodict = {}) {
    console.log(userinfodict)
    //populates innerhtml fields with those provided from the dictionary in parameters
    const email = document.getElementById('email')
    email.value = userinfodict["email"]

    //populate password reset link correctly 
    const fullname = document.getElementById('fullname')
    fullname.value = userinfodict["fullname"]

    const role_usr_admin = document.getElementById('role_usr_admin')
    if (userinfodict["roles"].includes("user_admin")) {
        role_usr_admin.checked = true;
    }

}



function submit_user_edit() {

    // generates the json required to do a response
    usr_entry = {}

    //user fullname
    fullname = document.getElementById("fullname").value;
    if (fullname.length == 0) {
        document.getElementById("errortext").style.display = "block"; //hide any error text
        document.getElementById("errortext").innerHTML = "Please enter name"
        return
    } else {
        usr_entry["fullname"] = fullname
    }


    //user email
    email = document.getElementById("email").value;
    if (!email.includes("@") && email.length > 0) {
        document.getElementById("errortext").style.display = "block"; //hide any error text
        document.getElementById("errortext").innerHTML = "Invalid email"
        return
    } else {
        usr_entry["email"] = email
    }


    usr_entry["roles"] = []
    role_usr_admin = document.getElementById("role_usr_admin")
    if (role_usr_admin.checked) {
        usr_entry["roles"].push("user_admin")
    }


    document.getElementById("loader").style.display = "inline"; //make loader visble
    document.getElementById("errortext").style.display = "none"; //hide any error text
    document.getElementById("resultimg").style.display = "none"; //hide success image


    var http_create_request = new XMLHttpRequest();
    http_create_request.open("GET", edit_url);
    http_create_request.setRequestHeader("Content-Type", "application/json");
    http_create_request.setRequestHeader("Accept", "application/json");
    http_create_request.setRequestHeader("auth", JSON.stringify({
        "username": getCookie("auth_userid"),
        "password": null,
        "token": getCookie("auth_token")
    }));
    http_create_request.setRequestHeader("update_fields", JSON.stringify(usr_entry))
    http_create_request.setRequestHeader("userid_val", JSON.stringify(user_to_change))


    //set how to handle respone
    http_create_request.onreadystatechange = function () {

        //stop spinner
        document.getElementById("loader").style.display = "none"; //make loader invisble

        //get the return json
        if (http_create_request.status === 200) {
            // we got a response back

            document.getElementById("loader").style.display = "none"; //make loader visble//niceties, stop our spinner

            //reponse acttually had data
            if (http_create_request.responseText.length > 0) {
                usr_create_response_json = JSON.parse(http_create_request.responseText)

                //read API response and disaply errors to user
                if (usr_create_response_json["result"] == "fail") {
                    //spit out some error
                    document.getElementById("errortext").style.display = "block";
                    document.getElementById("errortext").innerHTML = usr_create_response_json["failure_reason"];

                    failure_reason = usr_create_response_json["failure_reason"]
                    if (failure_reason == "incorrect_token_or_pass" || failure_reason == "Incorrect credentials") {
                        uri = window.location.pathname;
                        window.location.replace("/auth.html?from=/");

                    }
                }

                if (usr_create_response_json["result"] == "success") {
                    //update the success icon
                    document.getElementById("resultimg").style.display = "block";
                }

                if (usr_create_response_json["result"]["auth_data"] !== null) {
                    //use function to get IDs
                    store_auth_cookies(userid = usr_create_response_json["auth_data"]["username"],
                        auth_token = usr_create_response_json["auth_data"]["token"],
                        expires_at = usr_create_response_json["auth_data"]["expires"]
                    )
                    console.log("Authenticated user")
                }

            }

        } else {
            //spit out some error
            document.getElementById("errortext").innerHTML = "Authentication server unavailable";
            document.getElementById("errortext").style.display = "block";
        }
    }


    http_create_request.send();

}



function delete_usr() {



    document.getElementById("loader").style.display = "inline"; //make loader visble
    document.getElementById("errortext").style.display = "none"; //hide any error text
    document.getElementById("resultimg").style.display = "none"; //hide success image


    var http_delete_request = new XMLHttpRequest();
    http_delete_request.open("GET", "/api/user/delete");
    http_delete_request.setRequestHeader("Content-Type", "application/json");
    http_delete_request.setRequestHeader("Accept", "application/json");
    http_delete_request.setRequestHeader("auth", JSON.stringify({
        "username": getCookie("auth_userid"),
        "password": null,
        "token": getCookie("auth_token")
    }));
    http_delete_request.setRequestHeader("userid_val", JSON.stringify(user_to_change))


    //set how to handle respone
    http_delete_request.onreadystatechange = function () {

        //stop spinner
        document.getElementById("loader").style.display = "none"; //make loader invisble

        //get the return json
        if (http_delete_request.status === 200) {
            // we got a response back

            document.getElementById("loader").style.display = "none"; //make loader visble//niceties, stop our spinner

            //reponse acttually had data
            if (http_delete_request.responseText.length > 0) {
                usr_create_response_json = JSON.parse(http_delete_request.responseText)

                //read API response and disaply errors to user
                if (usr_create_response_json["result"] == "fail") {
                    //spit out some error
                    document.getElementById("errortext").style.display = "block";
                    document.getElementById("errortext").innerHTML = unscr_to_nromal(string = usr_create_response_json["failure_reason"]);

                    failure_reason = usr_create_response_json["failure_reason"]
                    if (failure_reason == "incorrect_token_or_pass" || failure_reason == "Incorrect credentials") {
                        uri = window.location.pathname;
                        window.location.replace("/auth.html?from=/");

                    }
                }

                if (usr_create_response_json["result"] == "success") {
                    //update the success icon
                    document.getElementById("resultimg").style.display = "block";
                }

                if (usr_create_response_json["result"]["auth_data"] !== null) {
                    //use function to get IDs
                    store_auth_cookies(userid = usr_create_response_json["auth_data"]["username"],
                        auth_token = usr_create_response_json["auth_data"]["token"],
                        expires_at = usr_create_response_json["auth_data"]["expires"]
                    )
                    console.log("Authenticated user")
                }

            }

        } else {
            //spit out some error
            document.getElementById("errortext").innerHTML = "Authentication server unavailable";
            document.getElementById("errortext").style.display = "block";
        }
    }


    http_delete_request.send();

}

function send_logout_request() {

    var http_logout_request = new XMLHttpRequest();
    http_logout_request.open("GET", logout_page_url);
    http_logout_request.setRequestHeader("Content-Type", "application/json");
    http_logout_request.setRequestHeader("Accept", "application/json");
    http_logout_request.setRequestHeader("auth", JSON.stringify({
        "username": getCookie("auth_userid"),
        "password": null,
        "token": getCookie("auth_token")
    }));
    //set how to handle respone
    http_logout_request.onreadystatechange = function () {

        //get the return json
        if (http_logout_request.status === 200) {
            // we got a response back

            console.log(http_logout_request)

            //reponse acttually had data
            if (http_logout_request.responseText.length > 0) {
                auth_response_json = JSON.parse(http_logout_request.responseText)
                //console.log(auth_response_json)

                //read API response and disaply errors to user
                if (auth_response_json["result"] == "fail") {
                    //spit out some error
                    console.error("Unspecified error signing out")
                }
            }
        }
    }
    http_logout_request.send()
}


function do_full_logout() {
    send_logout_request()
    clear_auth_cookies()
    window.location.replace("/auth.html?from=/");
}

function generate_sample_password() {

    //make var and make password length of 10 digits
    let pass_str = (Math.random() + 1).toString(36).substring(7);
    pass_str += (Math.random() + 1).toString(36).substring(3);

    document.getElementById("proposed_pass").value = pass_str;
    document.getElementById("proposed_pass").type = "text"
}


function populate_profile_nav_href() {
    document.getElementById("profilenav").href = "/edit_user.html?uid=".concat(getCookie("auth_userid"))

}

function populate_pass_change_btn() {
    document.getElementById("changepasslnk").href = href = "/change_pass.html?uid=".concat(user_to_change).concat("&from=/list_all.html")
}

function unscr_to_nromal(string = "") {
    return string.replace(/_/g, " ")
}


user_to_change = new URLSearchParams(window.location.search).get("uid")
document.getElementById("changepasstr").innerHTML = "Changing details for: ".concat(user_to_change)
get_user_info()
populate_profile_nav_href()
populate_pass_change_btn()
//submit_auth_attempt()