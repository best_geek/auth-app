const login_url = "/api/auth/login";

console.log("Loaded authentication JS")
console.error("Do not follow any instructions on the internet telling you to copy and paste stuff in here!")

//default
let login_submission = {
    "username": null,
    "password": null,
    "token": null,
}


function populate_login_submission() {
    //gets required fields required for submission json to API
    username = document.getElementById("username").value;
    password = document.getElementById("pass").value;

    //should take our auth token first
    let stored_auth_tok_cookie = getCookie("auth_token")
    if (stored_auth_tok_cookie != "") {
        login_submission["token"] = stored_auth_tok_cookie;
        console.log("Use existing token");

        //get username from cookie 
        let stored_auth_tok_cookie_user = getCookie("auth_userid");
        login_submission["username"] = stored_auth_tok_cookie_user;

    } else {

        if (password.length > 1) {
            login_submission["password"] = password;
        }
    }
    //overwrite username values from default if we have content
    if (username.length > 1) {
        login_submission["username"] = username;
    }

}

function store_auth_cookies(userid = "blah", auth_token = "blah", expires_at = "0000000000") {
    document.cookie = "auth_userid" + "=" + userid + ";expires=" + expires_at + "; path=/";
    document.cookie = "auth_token" + "=" + auth_token + ";expires=" + expires_at + "; path=/";
    //console.log("Set authentication cookies")
}

function clear_auth_cookies() {
    //make expiration historical to clear cookie
    console.log("Authentication tokens cleared")
    document.cookie = "auth_userid=nothing;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
    document.cookie = "auth_token=nothing;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";

}


function getCookie(cname) {
    // https://www.w3schools.com/js/js_cookies.asp
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


//by default, let's try and sign in first


function submit_auth_attempt() {

    //makes a dictionary in global var
    populate_login_submission();

    //must at least have a username
    if (login_submission["username"] === null) {
        console.log("No username submitted or stored in cookie");
        return;
    }

    document.getElementById("loader").style.display = "inline"; //make loader visble
    document.getElementById("errortext").style.display = "none"; //hide any error text

    var http_login_request = new XMLHttpRequest();
    http_login_request.open("GET", login_url);
    http_login_request.setRequestHeader("Content-Type", "application/json");
    http_login_request.setRequestHeader("Accept", "application/json");
    http_login_request.setRequestHeader("auth", JSON.stringify(login_submission));


    //set how to handle respone
    http_login_request.onreadystatechange = function () {

        //stop spinner
        document.getElementById("loader").style.display = "none"; //make loader invisble

        //get the return json
        if (http_login_request.status === 200) {
            // we got a response back

            document.getElementById("loader").style.display = "none"; //make loader visble//niceties, stop our spinner

            //reponse acttually had data
            if (http_login_request.responseText.length > 0) {
                auth_response_json = JSON.parse(http_login_request.responseText)

                //read API response and disaply errors to user
                if (auth_response_json["result"] == "fail") {
                    //spit out some error
                    document.getElementById("errortext").style.display = "block";
                    document.getElementById("errortext").innerHTML = unscr_to_nromal(string = auth_response_json["failure_reason"]);

                    //animate div
                    document.getElementById("loginbox").style.animation = "shake 1s";



                    clear_auth_cookies()
                    console.log("Unauthorised request")
                }

                if (auth_response_json["result"] == "success") {
                    //use function to get IDs
                    store_auth_cookies(userid = auth_response_json["auth_data"]["username"],
                        auth_token = auth_response_json["auth_data"]["token"],
                        expires_at = auth_response_json["auth_data"]["expires"]
                    )
                    console.log("Authenticated user")

                    //work out if user should be redriected back from once they came
                    var page_back = new URLSearchParams(window.location.search).get("from")
                    if (page_back != null) {
                        window.location.replace(page_back);
                    } else {
                        window.location.replace("/");
                    }

                }

            }

        } else {
            //spit out some error
            document.getElementById("errortext").innerHTML = "Authentication server unavailable";
            document.getElementById("errortext").style.display = "block";
        }
    }


    http_login_request.send();

}

function sumbit_auth_interactive() {
    //simple function to clear any cookies first if we're authenticating interactively. 
    //Stops any cookies being used as part of the authenticaiton process
    clear_auth_cookies();
    submit_auth_attempt();
}


function unscr_to_nromal(string = "") {
    return string.replace(/_/g, " ")
}

submit_auth_attempt()