const user_info_url = "/api/get/userinfo";
const login_page_url = "/login.html";
const logout_page_url = "/api/auth/logout_cookie";
let populted_info = false

console.error("Do not follow any instructions on the internet telling you to copy and paste stuff in here!")



let user_data = {
    "username": null,
    "fullname": null,
}

function store_auth_cookies(userid = "blah", auth_token = "blah", expires_at = "0000000000") {
    document.cookie = "auth_userid" + "=" + userid + ";expires=" + expires_at + "; path=/";
    document.cookie = "auth_token" + "=" + auth_token + ";expires=" + expires_at + "; path=/";
    console.log("Set authentication cookies")

}



function clear_auth_cookies() {
    //make expiration historical to clear cookie
    console.log("Authentication tokens cleared")
    document.cookie = "auth_userid=nothing;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
    document.cookie = "auth_token=nothing;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
}


function getCookie(cname) {
    // https://www.w3schools.com/js/js_cookies.asp
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


//by default, let's try and sign in first


function get_all_userinfo() {

    var http_get_alluserid_request = new XMLHttpRequest();
    http_get_alluserid_request.open("GET", "/api/user/list/all");
    http_get_alluserid_request.setRequestHeader("Content-Type", "application/json");
    http_get_alluserid_request.setRequestHeader("Accept", "application/json");
    http_get_alluserid_request.setRequestHeader("auth", JSON.stringify({
        "username": getCookie("auth_userid"),
        "password": null,
        "token": getCookie("auth_token")
    }));
    //set how to handle respone
    http_get_alluserid_request.onreadystatechange = function () {

        //get the return json
        if (http_get_alluserid_request.status === 200) {
            // we got a response back

            //reponse acttually had data
            if (http_get_alluserid_request.responseText.length > 0) {
                userinfo_response_json = JSON.parse(http_get_alluserid_request.responseText)

                //read API response and disaply errors to user
                if (userinfo_response_json["result"] == "fail") {
                    //direct user back
                    clear_auth_cookies()
                    console.log("Unauthorised request")
                    window.location.replace("/auth.html?from=/");
                }

                if (userinfo_response_json["result"]["auth_data"] !== null) {

                    //use function to get IDs
                    store_auth_cookies(userid = userinfo_response_json["auth_data"]["username"],
                        auth_token = userinfo_response_json["auth_data"]["token"],
                        expires_at = userinfo_response_json["auth_data"]["expires"]
                    )
                    console.log("Re-authenticated user")

                }

                if (userinfo_response_json["return_data"] !== null && populted_info == false) {

                    //draw items on screen 
                    //gernate a user entry with a for looop
                    len_results = userinfo_response_json["return_data"].length
                    for (var i = 0; i < len_results; i++) {
                        var item = userinfo_response_json["return_data"][i];
                        create_usr_entry(fullname = item["fullname"], userid = item["userid"])

                    }

                    //set our true flag
                    populted_info = true

                }
            }

        } else {
            //spit out some error
            //should rediriet user back to login page
            //window.location.replace("/auth.html?from=/");
        }
    }
    http_get_alluserid_request.send();
}



function send_logout_request() {

    var http_logout_request = new XMLHttpRequest();
    http_logout_request.open("GET", logout_page_url);
    http_logout_request.setRequestHeader("Content-Type", "application/json");
    http_logout_request.setRequestHeader("Accept", "application/json");
    http_logout_request.setRequestHeader("auth", JSON.stringify({
        "username": getCookie("auth_userid"),
        "password": null,
        "token": getCookie("auth_token")
    }));
    //set how to handle respone
    http_logout_request.onreadystatechange = function () {

        //get the return json
        if (http_logout_request.status === 200) {
            // we got a response back

            console.log(http_logout_request)

            //reponse acttually had data
            if (http_logout_request.responseText.length > 0) {
                auth_response_json = JSON.parse(http_logout_request.responseText)
                //console.log(auth_response_json)

                //read API response and disaply errors to user
                if (auth_response_json["result"] == "fail") {
                    //spit out some error
                    console.error("Unspecified error signing out")
                }
            }
        }
    }
    http_logout_request.send()
}


function create_usr_entry(fullname = "", userid = "hello") {
    //creates the html element listed below:

    //<a class=useroption id=manageprofile href="#" style="text-align: left;"><img src="/res/img/userico.svg" class="useroption"></img>
    //Nathan <p class="useroption">(nathan)</p></a>

    //set link addr
    const link = document.createElement("a");
    link.href = "/edit_user.html?uid=".concat(userid)
    link.className = "useroption"

    //show profile image
    const img = document.createElement("img")
    img.src = "/res/img/userico.svg"
    img.className = "useroption"
    link.appendChild(img)

    //add text to link before putting userid in bracket
    const append_link_text = document.createTextNode(fullname);
    link.appendChild(append_link_text);

    //create disaply element for userid
    const p_userid = document.createElement("p")
    p_userid.className = "useroption"
    const p_usrid_txt = document.createTextNode("(".concat(userid).concat(")"))
    p_userid.appendChild(p_usrid_txt)

    link.appendChild(p_userid)

    const maindiv = document.getElementById("contentbox");
    maindiv.appendChild(link)

}


function populate_profile_nav_href() {
    document.getElementById("profilenav").href = "/edit_user.html?uid=".concat(getCookie("auth_userid"))

}

function do_full_logout() {
    send_logout_request()
    clear_auth_cookies()
    window.location.replace("/auth.html?from=/");
}

populate_profile_nav_href()
get_all_userinfo()