const logout_page_url="/api/auth/logout_cookie";
const pass_change_url='/api/user/change/password'

console.log("Loaded authentication JS")
console.error("Do not follow any instructions on the internet telling you to copy and paste stuff in here!")

//default
let login_submission = {
    "username": null,
    "password":null,
    "token":null,
}


function populate_login_submission () {
    //gets required fields required for submission json to API
    username=document.getElementById("username").value;
    password=document.getElementById("pass").value;

    //should take our auth token first
    let stored_auth_tok_cookie = getCookie("auth_token")
    if (stored_auth_tok_cookie != "") {
        login_submission["token"]=stored_auth_tok_cookie;
        console.log("Use existing token");

        //get username from cookie 
        let stored_auth_tok_cookie_user = getCookie("auth_userid");
        login_submission["username"]=stored_auth_tok_cookie_user;

    } else {
        
        if (password.length >1) {
            login_submission["password"]=password;
        }
    }
    //overwrite username values from default if we have content
    if (username.length > 1){
        login_submission["username"]=username;
    }



   
}

function store_auth_cookies(userid="blah",auth_token="blah",expires_at="0000000000"){
    document.cookie = "auth_userid" + "=" + userid + ";expires=" + expires_at + "; path=/";
    document.cookie = "auth_token" + "=" + auth_token + ";expires=" + expires_at + "; path=/";
    //console.log("Set authentication cookies")
}

function clear_auth_cookies() {
    //make expiration historical to clear cookie
    console.log("Authentication tokens cleared")
    document.cookie = "auth_userid=nothing;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
    document.cookie = "auth_token=nothing;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";

}


function getCookie(cname) {
    // https://www.w3schools.com/js/js_cookies.asp
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }


//by default, let's try and sign in first


function submit_pass_change(){



    pass1_val = document.getElementById("pass1").value;
    pass2_val = document.getElementById("pass2").value;

    ///check if they match
    if (pass1_val != pass2_val){
        document.getElementById("errortext").innerHTML = "Passwords don't match";
        document.getElementById("errortext").style.display = "block";
        return
    }

    //check if length ok
    if (pass1_val.length < 8 ){
        document.getElementById("errortext").innerHTML = "Password not long enough";
        document.getElementById("errortext").style.display = "block";
        return
    }


    user_to_change= new URLSearchParams(window.location.search).get("uid")
    if (user_to_change == null){
        document.getElementById("errortext").innerHTML = "No user token with request";
        document.getElementById("errortext").style.display = "block";
        return
    }


    document.getElementById("loader").style.display = "inline"; //make loader visble
    document.getElementById("errortext").style.display = "none"; //hide any error text

    var http_pass_change_request = new XMLHttpRequest();
    http_pass_change_request.open("GET", pass_change_url);
    http_pass_change_request.setRequestHeader("Content-Type", "application/json");
    http_pass_change_request.setRequestHeader("Accept", "application/json");
    http_pass_change_request.setRequestHeader("auth", JSON.stringify({"username": getCookie("auth_userid"),
                                                "password":null,
                                                "token":getCookie("auth_token")}))
    
    http_pass_change_request.setRequestHeader("pass_val", pass1_val)
    http_pass_change_request.setRequestHeader("userid_val", user_to_change)




    //set how to handle respone
    http_pass_change_request.onreadystatechange = function () {
        
        //stop spinner
        document.getElementById("loader").style.display = "none"; //make loader invisble

        //get the return json
        if (http_pass_change_request.status === 200) {
            // we got a response back

            document.getElementById("loader").style.display = "none"; //make loader visble//niceties, stop our spinner

            //reponse acttually had data
            if (http_pass_change_request.responseText.length > 0){
                pass_response_json=JSON.parse(http_pass_change_request.responseText)

                //read API response and disaply errors to user
                if (pass_response_json["result"]=="fail"){
                    //spit out some error
                    document.getElementById("errortext").style.display = "block";
                    document.getElementById("errortext").innerHTML = unscr_to_nromal(string=pass_response_json["failure_reason"]);

                    //redirect back if error hints we're not authenticated
                    failure_reason=pass_response_json["failure_reason"]
                    console.log(failure_reason)
                    if (failure_reason == "incorrect_token_or_pass" || failure_reason == "Incorrect credentials") {
                        uri=window.location.pathname;
                        window.location.replace("/auth.html?from=/");

                    }
                }

                if (pass_response_json["result"]["auth_data"] !== null){                   
                    //use function to get IDs
                    store_auth_cookies(userid=pass_response_json["auth_data"]["username"],
                    auth_token=pass_response_json["auth_data"]["token"],
                    expires_at=pass_response_json["auth_data"]["expires"]
                    )
                    console.log("Authenticated user")
                }

                if (pass_response_json["result"]=="success"){
                    //send the user back from whence they commeth
                    var page_back= new URLSearchParams(window.location.search).get("from")
                    if (page_back != null){
                        window.location.replace(page_back);
                    } else {
                        window.location.replace("/");
                    }
                }

            } 

        } else {
                //spit out some error
                document.getElementById("errortext").innerHTML = "Authentication server unavailable";
                document.getElementById("errortext").style.display = "block";
        }
    }


    http_pass_change_request.send();

}

function send_logout_request(){
    
    var http_logout_request = new XMLHttpRequest();
    http_logout_request.open("GET", logout_page_url);
    http_logout_request.setRequestHeader("Content-Type", "application/json");
    http_logout_request.setRequestHeader("Accept", "application/json");
    http_logout_request.setRequestHeader("auth", JSON.stringify({"username": getCookie("auth_userid"),
                                                                "password":null,
                                                                "token":getCookie("auth_token")}));
    //set how to handle respone
    http_logout_request.onreadystatechange = function () {
        
        //get the return json
        if (http_logout_request.status === 200) {
            // we got a response back

            console.log(http_logout_request)

            //reponse acttually had data
            if (http_logout_request.responseText.length > 0){
                pass_response_json=JSON.parse(http_logout_request.responseText)
                //console.log(pass_response_json)

                //read API response and disaply errors to user
                if (pass_response_json["result"]=="fail"){
                    //spit out some error
                    console.error("Unspecified error signing out")
                }
            } 
        } 
    }
    http_logout_request.send()
}


function do_full_logout(){
    send_logout_request()
    clear_auth_cookies()
    window.location.replace("/auth.html")
}


function generate_sample_password(){

    //make var and make password length of 10 digits
    let pass_str = (Math.random() + 1).toString(36).substring(7);
    pass_str+=(Math.random() + 1).toString(36).substring(3);

    document.getElementById("pass1").value = pass_str;
    document.getElementById("pass2").value = pass_str;

    //show password in first box
    var pass1 = document.getElementById("pass1");
    pass1.type="text";

}

function sumbit_auth_interactive(){
    //simple function to clear any cookies first if we're authenticating interactively. 
    //Stops any cookies being used as part of the authenticaiton process
    clear_auth_cookies();
    submit_auth_attempt();
}

function populate_profile_nav_href(){
    document.getElementById("profilenav").href="/edit_user.html?uid=".concat(getCookie("auth_userid"))

}

function unscr_to_nromal (string=""){
    return string.replace(/_/g, " ")
}




user_to_change= new URLSearchParams(window.location.search).get("uid")
document.getElementById("changepasstr").innerHTML = "Changing password for: ".concat(user_to_change)

populate_profile_nav_href()
//submit_auth_attempt()